import axios from "axios"
import type { Method, AxiosRequestConfig } from "axios"
const service = axios.create({
  baseURL: "http://localhost:7001",
  timeout: 5000,
})
service.interceptors.response.use((response) => response.data)
type Data<T> = {
  code: string
  message: string
  data: T
}
const request = <T>(
  url: string,
  method: Method = "get",
  submitData: object,
  config?: AxiosRequestConfig
) => {
  return service.request<T, Data<T>>({
    url,
    method,
    [method.toLowerCase() === "get" ? "params" : "data"]: submitData,
    ...config,
  })
}
export default request
